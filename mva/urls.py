from django.contrib import admin
from django.urls import path, re_path, include
from django.conf.urls import url


urlpatterns = [
    path('admin/', admin.site.urls),
    re_path('api/', include('home.urls')),
    # url(r'^api-auth/', include('rest_framework.urls')),
]
