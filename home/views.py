from django.core.mail import EmailMessage
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Chapter, Section, Suggestion
from .serializers import ChaptersSerializer, SectionSerializer


class ChaptersListView(generics.ListAPIView):
    queryset = Chapter.objects.all()
    serializer_class = ChaptersSerializer


class SectionDetailView(generics.ListAPIView):
    serializer_class = SectionSerializer

    def get_queryset(self):
        sectionid = self.kwargs['sectionid']
        queryset = Section.objects.filter(chapter__id=sectionid)
        return queryset


class FavoriteDeatilView(generics.ListAPIView):
    serializer_class = SectionSerializer

    def get_queryset(self):
        sectionids = self.request.query_params.getlist('sectionIds')
        queryset = Section.objects.filter(id__in=sectionids)
        return queryset


class CommonSectionsView(generics.ListAPIView):
    serializer_class = SectionSerializer

    def get_queryset(self):
        queryset = Section.objects.filter(common=True)
        return queryset


@api_view(['POST'])
def mail_processing(request):
    if request.method == 'GET':
        return Response('use post request')
    if request.method == 'POST':
        req = request.query_params
        to_email = req.get('email')
        sectionid = req.get('sectionid')
        suggestion = req.get('suggestion')
        print('to_email: ',to_email)
        print('sectionid: ', sectionid)
        print('suggestion: ', suggestion)
        if to_email is not None and suggestion is not '' and sectionid is not None:
            mail_subject = 'Thankyou For Suggestion'
            message = f'This is auto generated email. We will look at you suggestion. your suggestion is: {suggestion}'
            email = EmailMessage(mail_subject, message, to=[to_email])
            resp = email.send(fail_silently=False)

            db_object = Suggestion(section_id=int(sectionid),
                                   suggestion=suggestion, userEmail=to_email)
            db_object.clean_fields()
            db_object.save(force_insert=True)
            return Response('mail sent')

    return Response('mail cant sent')


@api_view(['GET'])  
def about(request):
    texts = 'this is about page response from server'
    return Response(texts)


@api_view(['GET'])
def disclaimer(request):
    texts = 'this is disclaimer page response from server'
    return Response(texts)
