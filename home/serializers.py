from rest_framework import serializers
from .models import Chapter, Section


class ChaptersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chapter
        fields = ('id', 'title')


class SectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Section
        fields = ('id', 'title', 'description')
