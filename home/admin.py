from django.contrib import admin
from .models import Chapter, Section, Suggestion


@admin.register(Chapter)
class ChapterAdmin(admin.ModelAdmin):
    list_display = ['title']


@admin.register(Section)
class SectionAdmin(admin.ModelAdmin):
    list_display = ['title', 'description', 'common', 'chapter']


@admin.register(Suggestion)
class SuggestionAdmin(admin.ModelAdmin):
    list_display = ['section', 'suggestion', 'userEmail']
