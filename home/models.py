from django.db import models


class Chapter(models.Model):
    title = models.CharField(max_length=50)

    def __str__(self):
        return '{}'.format(self.title)


class Section(models.Model):
    title = models.CharField(max_length=150)
    description = models.TextField()
    common = models.BooleanField(default=False)
    chapter = models.ForeignKey(Chapter, on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.title)


class Suggestion(models.Model):
    section = models.ForeignKey(Section, on_delete=models.CASCADE)
    suggestion = models.TextField()
    userEmail = models.EmailField()

    def __str__(self):
        return '{}'.format(self.section.title)
