from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from .views import (ChaptersListView, CommonSectionsView, FavoriteDeatilView,
                    SectionDetailView, mail_processing, about, disclaimer)


urlpatterns = [
    path('chapters/', ChaptersListView.as_view(), name="chapters"),
    path('chapters/<int:sectionid>/',
         SectionDetailView.as_view(), name="section_detail"),
    path('fav/', FavoriteDeatilView.as_view(), name='fav_list'),
    path('common/', CommonSectionsView.as_view(), name='common_section'),
    path('sugg/', csrf_exempt(mail_processing), name='mail_processing'),
    path('about/', about, name='about'),
    path('disclaimer/', disclaimer, name='disclaimer'),
]
